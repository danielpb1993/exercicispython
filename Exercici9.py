a = int(input("Escriu un numero: "))
b = int(input("Escriu el número divisor: "))

while b == 0:
    print("Impossible dividir per 0")
    b = int(input("Escriu un nou número divisor: "))

if (a % b) == 0:
    print("Es exacte")
else:
    print("No es exacte")