a = int(input("Escriu el nombre el paraules que vols introduir a la primera llista: "))

p1 = [a for x in range(a)]

for x in range(a):
    p1[x] = str(input("Quina paraula vols introduir en la posicio num {}: ".format(x)))

b = int(input("Escriu el nombre el paraules que vols introduir a la segona llista: "))

p2 = [b for x in range(b)]

for x in range(b):
    p2[x] = str(input("Quina paraula vols introduir en la posicio num {}: ".format(x)))

for x in p1:
    for y in p2:
        if x == y:
            p1.remove(x)

print(p1)