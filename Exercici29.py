llista = []
with open('exercici29.txt') as file:
    for linia in file:
        for paraula in linia.split():
            if not llista.__contains__(paraula.lower()):
                llista.append(paraula.lower())

print(sorted(llista))