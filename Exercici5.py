print("Exercici 5a")
vocals = ['a', 'e', 'i', 'o', 'u']
print(vocals)
numModificar = int(input("Quin número d'element de la llista vols modificar? "))
nouValor = input("Amb quin nou valor vols substituir-lo? ")
vocals[numModificar] = nouValor
print(vocals)

print("Exercici 5b")
lletraModificar = input("Quina lletra vols modificar? ")
nouValor = input("Amb quin valor vols substituir-la? ")
vocals[vocals.index(lletraModificar)] = nouValor
print(vocals)