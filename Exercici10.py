print("L'àrea de quina figura vols calcular: ")
print("1. Cercle")
print("2. Triangle")

res = int(input())

if res == 1:
    radi = float(input("Introdueix el radi del cercle: "))
    areaC = 3.141516 * radi ** 2
    print("L'area del cercle es {}".format(areaC))
else:
    alçada = float(input("Introdueix l'alçada del triangle: "))
    base = float(input("Introdueix la base del triangle: "))
    areaT = alçada * base / 2
    print("L'area del triangle es {}".format(areaT))