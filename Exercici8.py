llista1 = ["hola", "com", "va", "tiu"]
llista2 = ["hola", "que", "tal", "tiu"]
aLesDues = []
aLaPrimera = []
aLaSegona = []
aAlguna = []
for element in llista1:
    aAlguna.append(element)
    if element in llista2:
        aLesDues.append(element)
    elif element not in llista2:
        aLaPrimera.append(element)

for element in llista2:
    if element not in llista1:
        aLaSegona.append(element)

print("Paraules que apareixen a les dues llistes: {}".format(aLesDues))
print("Paraules que apareixen a la primera llista però no a la segona: {}".format(aLaPrimera))
print("Paraules que apareixen a la segona llista però no a la primera: {}".format(aLaSegona))
print("Paraules que apareixen alguna vegada a les dues llistes: {}".format(aAlguna))